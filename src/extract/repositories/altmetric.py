"""Altmetric"""
import json
from repository import Repository


class Altmetric(Repository):
    """Altmetric"""
    base = "https://api.altmetric.com/v1/doi/"

    def __init__(self, *args, **kwargs):
        super(Altmetric, self).__init__(*args, **kwargs)

    def request_url(self, doi):
        """Construct request string"""
        req = Altmetric.base + doi            

        return req

    def request_handler(self, response):
        try:
            json_str = json.loads(response)
        except:
            json_str = None

        return json_str


if __name__ == '__main__':
    def main():
        """main"""
        doi = "10.1016/S2352-3026(18)30196-0"#"10.1111/j.1365-2966.2007.11913.x"

        rep = Altmetric()

        req_dict, status = rep.request(doi)
        
        print status

        print req_dict
        #print req_dict['cited_by_policies_count']

    main()
