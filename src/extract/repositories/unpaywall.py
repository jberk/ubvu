"""Unpaywall"""
import json
from repository import Repository


class Unpaywall(Repository):
    """Unpaywall"""
    base = "https://api.unpaywall.org/v2/"
    tail = "?email="

    def __init__(self, email=None, *args, **kwargs):
        super(Unpaywall, self).__init__(*args, **kwargs)
        # define base request URL
        self._email = email

    def request_url(self, doi):
        """Construct request string"""
        req = Unpaywall.base \
            + doi            \
            + Unpaywall.tail \
            + self._email

        return req

    def request_handler(self, response):
        try:
            json_str = json.loads(response)
        except:
            json_str = None

        return json_str


if __name__ == '__main__':
    def main():
        """main"""
        email = "jberk@absu.nl"
        doi = "10.1111/j.1365-2966.2007.11913.x"

        rep = Unpaywall(email)

        req_dict, status = rep.request(doi)

        print req_dict, status
        print req_dict['is_oa']

    main()
