"""Repository"""
import timeit
import time
import requests

class Repository(object):
    """Repository"""
    def __init__(self, rate=1):
        self._rate = rate #calls/sec
        self._last_req = timeit.default_timer()
        self._base_url = ''

    def request_url(self, term):
        """interface specification"""
        raise NotImplementedError(
            "Requires implementation of request_url(term)")

    def request_handler(self, response):
        """nilpotent"""
        return response

    def request(self, term):
        """RESTful request"""
        req_url = self.request_url(term)

        try:
            min_sleep = 1./self._rate
        except:
            min_sleep = 0.
        # restrict to request rate
        deltat = -min(timeit.default_timer() - self._last_req - min_sleep, 0)
        time.sleep(deltat)

        response = requests.get(req_url)

        self._last_req = timeit.default_timer()

        return (self.request_handler(response.text), response.status_code)

    @property
    def rate(self):
        """rate property."""
        return self._rate

    @rate.setter
    def rate(self, value):
        self._rate = value

    def __enter__(self):
        """context manager enter"""
        return self

    def __exit__(self, type, value, traceback):
        """context manager exit"""
        return True
