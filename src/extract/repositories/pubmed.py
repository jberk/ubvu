"""PubMed"""
import xmltodict # convert the XML response
from repository import Repository


class PubMed(Repository):
    """PubMed"""
    base       = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi"\
               + "?db="
    term       = "&term="
    __retstart = "&retstart="
    __retmax   = "&retmax="

    def __init__(self, *args, **kwargs):
        super(PubMed, self).__init__(*args, **kwargs)
        # define base request URL
        self._db       = "pubmed"
        self._retstart = 0
        self._retmax   = 200 #<=1000

    @property
    def retstart(self):
        """retstart property."""
        return self._retstart

    @retstart.setter
    def retstart(self, value):
        self._retstart = value

    @property
    def retmax(self):
        """retmax property."""
        return self._retmax

    @retmax.setter
    def retmax(self, value):
        self._retmax = value

    def request_url(self, term):
        """Construct request string"""
        req =  PubMed.base      + self._db            \
            + PubMed.__retstart + str(self._retstart) \
            + PubMed.__retmax   + str(self._retmax)   \
            + PubMed.term       + term

        return req

    def request_handler(self, response):
        try:
            xml_str = xmltodict.parse(response)
        except:
            xml_str = None

        return xml_str


class IDConv(Repository):
    """IDConv"""
    base = "https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/"\
         + "?ids="

    def __init__(self):
        super(IDConv, self).__init__()

    def request_url(self, ids):
        ids_str = ','.join(ids)
        """Construct request string"""
        req = IDConv.base + ids_str
        
        return req

    def request_handler(self, response):
        try:
            xml_str = xmltodict.parse(response)
        except:
            xml_str = None

        return xml_str


class EFetch(Repository):
    """EFetch"""
    base    = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db="
    __id    = "&id="
    retmode = "&retmode="

    def __init__(self):
        super(EFetch, self).__init__()
        self._db       = "pubmed"
        self._retmode  = "xml"


    def request_url(self, ids):
        ids_str = ','.join(ids)
        """Construct request string"""
        req = EFetch.base + self._db        \
            + EFetch.__id + ids_str         \
            + EFetch.retmode + self._retmode
       
        return req

    def request_handler(self, response):
        try:
            xml_str = xmltodict.parse(response)
        except:
            xml_str = None

        return xml_str


if __name__ == '__main__':
    def main():
        """main"""
        term_1 = "(((\"2018/01/01\"[Date - Publication] :"\
               +" \"2018/12/31\"[Date - Publication]) AND"\
               +" lymphoma[MeSH]) AND leukemia[MeSH])"

        term_2 = "((\"2018/01/01\"[Date - Publication] :" \
                +"\"2018/12/31\"[Date - Publication]) AND"\
                +" digestive system neoplasms[MeSH])"

        rep = PubMed()
        rep.retmax = 2

        #print rep.request_url(term_1)

        req_dict, status = rep.request(term_1)
        
        print req_dict
        print req_dict['eSearchResult']['IdList']['Id']

        ids = req_dict['eSearchResult']['IdList']['Id']

        conv = EFetch()
        print conv.request_url(ids)
        conv_dict, status = conv.request(ids)

        # where to find in tree
        articles = conv_dict['PubmedArticleSet']['PubmedArticle']
        selector = lambda a: a['MedlineCitation']['Article']\
                              ['ELocationID'][1]['#text']

        # extract the dois
        dois = [selector(articles[idx]) for idx in range(len(articles))]
        print dois


    main()
