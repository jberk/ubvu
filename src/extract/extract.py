"""Extract records PubMed"""
from __future__ import print_function
import sys
import types
import time
import json
from repositories import PubMed, EFetch, Unpaywall, Altmetric

def get_request(requestor, name, term
              , recoverable_server_errors = [500, 502, 503, 504]):

    rec_dict, status = requestor.request(term)
    while (status != 200):
        if status not in recoverable_server_errors:
            raise Exception('unexpected status '+name+': {}'.format(status))
        elif status in recoverable_server_errors:
            print('unexpected status '+name+': {}'.format(status)
                , file=sys.stderr)
            time.sleep(60*5) # sleep 5 min
        rec_dict, status = requestor.request(term)

    return rec_dict

def get_records(term, chunk=200, email = "jberk@absu.nl"):
    """quick function, maybe absorb in generic driver class?:"""
    pm = PubMed()
    pm.retmax   = chunk
    pm.retstart = 0

    conv = EFetch()

    am = Altmetric(rate=1)

    upw = Unpaywall(email, rate=1)

    recoverable_server_errors = [500, 502, 503, 504]

    # selectors on record tree
    #doi = lambda a: a['MedlineCitation']['Article']\
    #                      ['ELocationID'][1]['#text']
    def doi(a):
        # (try) find DOI among location ids
        
        try:
            loc_ids = a['MedlineCitation']['Article']['ELocationID']
        
            if isinstance(loc_ids, types.DictType):
                loc_ids = [loc_ids]

            for loc_id in loc_ids:
                if loc_id['@EIdType'] == 'doi':
                    return loc_id['#text']
        except:
            pass

        # no DOI found (e.g. patient case report)
        return None

    pmid = lambda a: a['MedlineCitation']['PMID']['#text']


    records = {}

    total_count = 0
    ret_len     = 0
    while (pm.retstart < total_count) or (len(records) == 0):
        # query PubMed
        req_dict = get_request(pm, 'PubMed', term)

        # if empty record set: stop
        try:
            ids = req_dict['eSearchResult']['IdList']['Id']
        except:
            break
        # ensure list
        if isinstance(ids, types.StringTypes):
            ids = [ids]
        total_count = req_dict['eSearchResult']['Count']

        # convert PMIDs to DOIs
        conv_dict = get_request(conv, 'EFetch', ids)

        # where to find in tree
        articles = conv_dict['PubmedArticleSet']['PubmedArticle']

        # extract the dois, ensure list
        if isinstance(articles, types.DictType):
            dois = [doi(articles)]
            pmids = [pmid(articles)]
            ret_len = 1
        else:
            ret_len = len(articles)
            dois = [doi(articles[idx]) for idx in range(ret_len)]
            pmids = [pmid(articles[idx]) for idx in range(ret_len)]


        rec_data = []
        # only 1 doi/call
        for d in dois:
            if d is None:
                pol_counts  = None
                oa_flag     = None
                read_counts = None
            else:
                # altmetric
                status = recoverable_server_errors[0]
                while status in recoverable_server_errors:
                    req_dict, status = am.request(d)
                    #print doi, req_dict, status

                    pol_counts = 0
                    read_counts = 0
                    if status == 200:
                        # assume 0 if not present
                        try:
                            pol_counts = req_dict['cited_by_policies_count']
                        except:
                            pol_counts = None
                        try:
                            read_counts = req_dict['readers_count']
                        except:
                            read_counts = None
                    elif status == 404:
                        pol_counts = None
                        read_counts = None
                    elif status not in recoverable_server_errors:
                        raise Exception(
                            'unexpected status Altmetric: {}'.format(status))
                    if status in recoverable_server_errors:
                        time.sleep(60*5)

                # unpaywall
                status = recoverable_server_errors[0]
                while status in recoverable_server_errors:
                    req_dict, status = upw.request(d)
                    #print doi, req_dict, status
                    oa_flag = None
                    if status == 200:
                        oa_flag = req_dict['is_oa']
                    elif status == 404:
                        oa_flag = None
                    elif status not in recoverable_server_errors:
                        raise Exception(
                            'unexpected status Unpaywall: {}'.format(status))
                    if status in recoverable_server_errors:
                        time.sleep(60*5)


            drec = {'cited_by_policies_count': pol_counts
                   ,'readers_count'          : read_counts
                   ,'is_oa'                  : oa_flag
                   ,'doi'                    : d}

            rec_data.append(drec)

        # add records
        records.update(dict(zip(pmids, rec_data)))

        pm.retstart = (pm.retstart + ret_len)
        print(len(records), ret_len, pm.retstart, total_count
              , file=sys.stderr)
        #print pmids[-1], dois[-1]
        #print dict(zip(pmids, rec_data))

    return records



if __name__ == '__main__':
    def main(chunk=200):
        """main"""

        year = str(sys.argv[1])

        term_1 = "(((\""+year+"/01/01\"[Date - Publication] :"\
               +" \""+year+"/12/31\"[Date - Publication]) AND"\
               +" lymphoma[MeSH]) AND leukemia[MeSH])"

        term_2 = "((\""+year+"/01/01\"[Date - Publication] :" \
                +"\""+year+"/12/31\"[Date - Publication]) AND"\
                +" digestive system neoplasms[MeSH])"

        term_3 = "((\""+year+"/01/01\"[Date - Publication] :" \
                +"\""+year+"/12/31\"[Date - Publication]) AND"\
                +" brain neoplasms[MeSH])"


        records_1 = get_records(term_1, chunk)
        records_3 = get_records(term_3, chunk)

        records = {term_1: records_1, term_3: records_3}

        # dump dict to json file
        json_str = json.dumps(records, indent=4)
        sys.stdout.write(json_str)


    main()
