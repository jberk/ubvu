"""Digital Object Identifier module"""


class DOI(object):
    """DOI interface class"""
    r_sep = '.'
    s_sep = '/'

    def __init__(self, doi_str=None, ddir=10, registrant=None, suffix=None):
        """initialiser"""
        self._doi = {}

        if doi_str is not None:
            (ddir, _, rem) = doi_str.partition(DOI.r_sep)
            (registrant, _, suffix) = rem.partition(DOI.s_sep)

        if ddir is not None:
            self._doi['dir'] = ddir
        if registrant is not None:
            self._doi['registrant'] = registrant
        if suffix is not None:
            self._doi['suffix'] = suffix.upper()


    def __str__(self):
        """string repr"""
        elems = map(self._doi.get, ['dir', 'registrant', 'suffix'])
        ret = ''.join([str(el)
                       for tup in zip(elems, [DOI.r_sep, DOI.s_sep, ''])
                       for el in tup
                      ]
                     )
        return ret

    __repr__ = __str__

    @property
    def dir(self):
        """dir"""
        return self._doi['dir']

    @property
    def registrant(self):
        """registrant"""
        return self._doi['registrant']

    @property
    def suffix(self):
        """suffix"""
        return self._doi['suffix']


if __name__ == "__main__":
    def main():
        """main"""
        doi = DOI("10.1111/1468-0262.00399")

        print(doi)
        print(doi.dir)
        print(doi.registrant)
        print(doi.suffix)

    main()
