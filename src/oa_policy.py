import sys
import json
from scipy import stats
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import plot
import plotly.io as pio


# sanitize numbers
map_to_nr = lambda v: v if v is not None else 0

def get_oa_count(records, search_terms):
    sel_oa = lambda rec: map_to_nr(rec['is_oa'])
    map_oa = lambda recs: map(lambda i: sel_oa(i[1]), recs.items())

    oa_counts = [sum(map_oa(records[term])) for term in search_terms]

    # total with flag
    sel_oa_flag = lambda rec: rec['is_oa'] is not None
    map_oa_flag = lambda recs: map(lambda i: sel_oa_flag(i[1]), recs.items())

    oa_flagged = [sum(map_oa_flag(records[term])) for term in search_terms]


    return (oa_counts, oa_flagged)


def get_pc_count(records, search_terms):
    sel_pol = lambda rec: map_to_nr(rec['cited_by_policies_count'])
    map_pol = lambda recs: map(lambda i: sel_pol(i[1]), recs.items())

    pol_counts = [sum(map_pol(records[term])) for term in search_terms]

    # total with flag
    sel_pol_flag = lambda rec: rec['readers_count'] is not None # sic
    map_pol_flag = lambda recs: map(lambda i: sel_pol_flag(i[1]), recs.items())

    pol_flagged = [sum(map_pol_flag(records[term])) for term in search_terms]

    return (pol_counts, pol_flagged)


def get_pc_oa_count(records, search_terms):
    sel_oa = lambda rec: map_to_nr(rec['is_oa'])
    sel_n_oa = lambda rec: map_to_nr((rec['is_oa'] == 0))
    sel_pol = lambda rec: map_to_nr(rec['cited_by_policies_count'])

    map_oa_pol = lambda recs: map(
            lambda i: sel_pol(i[1]) if sel_oa(i[1]) == 1 else 0, recs.items())
    map_n_oa_pol = lambda recs: map(
            lambda i: sel_pol(i[1]) if sel_n_oa(i[1]) == 1 else 0, recs.items())


    oa_pol_counts = [sum(map_oa_pol(records[term])) for term in search_terms]
    n_oa_pol_counts = [sum(map_n_oa_pol(records[term]))
                            for term in search_terms]

    return (oa_pol_counts, n_oa_pol_counts)


def get_rc_count(records, search_terms):
    sel_rc = lambda rec: map_to_nr(rec['readers_count'])
    map_rc = lambda recs: map(lambda i: sel_rc(i[1]), recs.items())

    rc_counts = [sum(map_rc(records[term])) for term in search_terms]

    # total with flag
    sel_rc_flag = lambda rec: rec['readers_count'] is not None
    map_rc_flag = lambda recs: map(lambda i: sel_rc_flag(i[1]), recs.items())

    rc_flagged = [sum(map_rc_flag(records[term])) for term in search_terms]


    return (rc_counts, rc_flagged)


def get_doi_pc_count(records, search_terms):
    sel_doi = lambda rec: rec['doi']
    sel_pol = lambda rec: map_to_nr(rec['cited_by_policies_count'])
    map_pol = lambda recs: map(lambda i: sel_pol(i[1]), recs.items())

    pol_dois = [[(sel_doi(i[1]), sel_pol(i[1])) for i in records[term].items()
                        if sel_pol(i[1]) > 0]
                    for term in search_terms]
    return pol_dois


def bubble_chart(x, y, size, labels=None, x_range=(0,100), y_range=(0,100),
                 name='bubble.pdf', colscale=None, label='size: '):

    colrange = [(153, 204, 255), (255, 102, 102)]
    if colscale is None:
        scale = len(x) - 1 if len(x) > 0 else 1
        colscale = [[str((colrange[1][0]-colrange[0][0])/scale*(idx)
                        + colrange[0][0])
                   , str((colrange[1][1]-colrange[0][1])/scale*(idx)
                        + colrange[0][1])
                   , str((colrange[1][2]-colrange[0][2])/scale*(idx)
                        + colrange[0][2])
                    ] for idx in range(len(x))]
   
    if labels is None:
        labels = [label+str(s) for s in size]

    scat = go.Scatter(
        x=x,
        y=y,
        text=labels,
        mode='markers+text',
        marker=dict(
            color=['rgb('+','.join(rgb)+')' for rgb in colscale],
            size=size,
        )
    )

    
    data = [scat]
    layout = go.Layout(
        font=dict(size=16, color='#000000'),
        title='2014 - 18 publications',
        xaxis=dict(
            title='Open Access (%)',
            gridcolor='rgb(255, 255, 255)',
            range=x_range,
            zerolinewidth=1,
            ticklen=5,
            gridwidth=2,
        ),
        yaxis=dict(
            title='Policy document citations (%)',
            gridcolor='rgb(255, 255, 255)',
            range=y_range,
            zerolinewidth=1,
            ticklen=5,
            gridwidth=2,
        ),
        #paper_bgcolor='rgb(243, 243, 243)',
        #plot_bgcolor='rgb(243, 243, 243)',
    )

    out = go.Figure(data=data, layout=layout)


    pio.write_image(out, 'bubble.pdf')



if __name__ == '__main__':
    years = range(2014, 2019)

    # get records
    records = {}

    for year in years:
        file_name = str(year)+'_A_B.json' 
        with open(file_name) as json_file:
            records[year] = json.load(json_file)

        print(sorted(records[year].keys())[0])
        print(len(records[year][sorted(records[year].keys())[0]]))
        print(sorted(records[year].keys())[1])
        print(len(records[year][sorted(records[year].keys())[1]]))

        print('')

    search_count = len(records[years[0]].keys())


    # get open access counts
    oa_counts = None
    pc_counts = None
    counts    = None
    pc_oa_counts = None
    for year in years:
        search_terms = sorted(records[year].keys())
        rec = records[year]
        oa = get_oa_count(rec, search_terms)
        pc = get_pc_count(rec, search_terms)

        oa_pc = get_pc_oa_count(rec, search_terms)

        if oa_counts is None:
            oa_counts = list(oa)
        else:
            for idx in range(search_count):
                for jdx in range(len(oa_counts[idx])):
                    oa_counts[idx][jdx] = oa_counts[idx][jdx] + oa[idx][jdx]
        if pc_counts is None:
            pc_counts = list(pc)
        else:
            for idx in range(search_count):
                for jdx in range(len(pc_counts[idx])):
                    pc_counts[idx][jdx] = pc_counts[idx][jdx] + pc[idx][jdx]

        if counts is None:
            counts = [len(rec[search_terms[idx]])
                        for idx in range(search_count)]
        else:
            for idx in range(search_count):
                counts[idx] = counts[idx] + len(rec[search_terms[idx]])

        if pc_oa_counts is None:
            pc_oa_counts = list(oa_pc)
        else:
            for idx in range(search_count):
                for jdx in range(len(pc_oa_counts[idx])):
                    pc_oa_counts[idx][jdx] = pc_oa_counts[idx][jdx] \
                                           + oa_pc[idx][jdx]


    res = {idx:
            {'oa_frac': float(oa_counts[0][idx])/oa_counts[1][idx]
                            if oa_counts[1][idx] > 0 else None
           , 'pc_frac': float(pc_counts[0][idx])/pc_counts[1][idx]
                            if pc_counts[1][idx] > 0 else None
           , 'total_count' : counts[idx]
           , 'pc_oa_count' : pc_oa_counts[0][idx]
           , 'pc_n_oa_count' : pc_oa_counts[1][idx]
            }
            for idx in range(search_count)
          }

    print res
    print oa_counts
    print pc_counts

    # draw bubble chart
    oa_percs = [100*res[idx]['oa_frac']
                    for idx in range(search_count)]
    pc_percs = [100*res[idx]['pc_frac']
                    for idx in range(search_count)]

    #ref_size = 15 #  2018
    ref_size = 150 # all
    pub_count = [float(oa_counts[1][idx])/ref_size
                    for idx in range(search_count)
                ]

    labels = ['A', 'B']
    bubble_chart(oa_percs, pc_percs, pub_count, labels
               , x_range=(30,50), y_range=(-2,10)
               #, x_range=(0,40), y_range=(-10,10) # 2018
                )

    print "graph params:", oa_percs, pc_percs, pub_count


    for idx in range(search_count):
        # reference frequecny: policy citations without OA
        # scaled with number of publications not OA
        n_oa_cnt = oa_counts[1][idx] - oa_counts[0][idx]
        oa_cnt = oa_counts[0][idx]
        p = float(res[idx]['pc_n_oa_count'])/n_oa_cnt
        print n_oa_cnt, p, res[idx]['pc_n_oa_count'], oa_counts[1][idx]
        pval = stats.binom_test(float(res[idx]['pc_oa_count'])
                              , n=oa_counts[0][idx]
                              , p=p
                              , alternative='greater'
                               )
        if pval < 0.05:
            print pval, "null hypothesis rejected"
        else:
            print pval, "null hypothesis cannot be rejected"

    # searches combined
    n_oa_cnt = sum(oa_counts[1][:search_count])\
             - sum(oa_counts[0][:search_count])
    oa_cnt = sum(oa_counts[0][:search_count])
    p = float(sum([res[i]['pc_n_oa_count']
                        for i in range(search_count)]))/n_oa_cnt
    
    print n_oa_cnt, p, sum([res[i]['pc_n_oa_count']
                            for i in range(search_count)]),
    print sum(oa_counts[1][:search_count])

    pval = stats.binom_test(float(sum([res[i]['pc_oa_count']
                                for i in range(search_count)]))
                          , n=sum(oa_counts[0][:search_count])
                          , p=p
                          , alternative='greater'
                           )
    if pval < 0.05:
        print pval, "null hypothesis rejected"
    else:
        print pval, "null hypothesis cannot be rejected"

