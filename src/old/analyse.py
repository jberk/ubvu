import sys
import json
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import plot
import plotly.io as pio



if __name__ == '__main__':
    records = json.load(sys.stdin)

    # [term][pmid][readers_count | is_oa | doi | cited_by_policies_count]

    search_terms = records.keys()
    counts = [len(records[term]) for term in search_terms]

    print counts

    map_to_nr = lambda v: v if v is not None else 0

    
    sel_oa = lambda rec: map_to_nr(rec['is_oa'])
    map_oa = lambda recs: map(lambda i: sel_oa(i[1]), recs.items())

    oa_counts = [sum(map_oa(records[term])) for term in search_terms]

    print oa_counts


    sel_pol = lambda rec: map_to_nr(rec['cited_by_policies_count'])
    map_pol = lambda recs: map(lambda i: sel_pol(i[1]), recs.items())

    pol_counts = [sum(map_pol(records[term])) for term in search_terms]

    print pol_counts


    sel_rc = lambda rec: map_to_nr(rec['readers_count'])
    map_rc = lambda recs: map(lambda i: sel_rc(i[1]), recs.items())

    rc_counts = [sum(map_rc(records[term])) for term in search_terms]

    print rc_counts

    # get dois with policy counts
    sel_doi = lambda rec: rec['doi']
    map_pol = lambda recs: map(lambda i: sel_pol(i[1]), recs.items())

    pol_dois = [[(sel_doi(i[1]), sel_pol(i[1])) for i in records[term].items()
                        if sel_pol(i[1]) > 0]
                    for term in search_terms]

    print pol_dois

    # draw bubble chart
    # abscissa (x) : open access fraction
    # ordinate (y) : policy citation count
    # scale        : publication count for search terms

    fig = go.Figure()
    fig.add_scatter(
        x=[1, 2, 3, 4],
        y=[10, 11, 12, 13],
        text=['A<br>size: 40', 'B<br>size: 60', 'C<br>size: 80', 'D<br>size: 100'],
        mode='markers+text',
        marker=dict(
            color=['rgb(93, 164, 214)', 'rgb(255, 144, 14)',  'rgb(44, 160, 101)', 'rgb(255, 65, 54)'],
            size=[40, 60, 80, 100],
        )
    )

    #data = [trace0]
    #plot(data, filename='bubblechart-text.html')
    pio.write_image(fig, 'bubble.pdf')



