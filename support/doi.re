dir = (10)

registrant = \d+

suffix = re.compile(ur'[\u0020-\ud7ff]', re.DEBUG)

This specifically excludes the
control character ranges 0x00-0x1F and 0x80-0x9F

0020 = ' '# space

 Universal Character Set (UCS-2), of ISO/IEC 10646,



# shortcut:
{dir}.{registrant}/{suffix}
